import numpy as np
from scipy.ndimage import convolve
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML
from numba import jit


def laplace(N,L):
    h = L/N
    L = 1/(6 * h*h) * np.array([
        [1, 4, 1],
        [4,-20, 4],
        [1, 4, 1]
    ])
    return L


def solve_system(func_du, func_dv, u_start, v_start, t_end, model, epsilon=0, dt=1e-3, output_steps=200):
    U = []           # output as 3D matrix
    V = []   
    
    u = u_start      # N*N Matrix with startvalues for excitation
    v = v_start
    
    t = 0
    t_output = np.linspace(0, t_end, output_steps)
    for t_next in t_output:
        while t < t_next:
            u = u + dt * func_du(u, v)
#           v = v + dt * func_dv(u,v)         # Euler step method
            if model == "barkley":            # Rush-Larsen method
                v = (v - u) * np.exp((-1) * dt) + u
            elif model == "fitz":
                v = (v - u) * np.exp((-1) * epsilon * dt) + u
            t += dt
        U.append(u)
        V.append(v)
    return U, V
