# Spiral Tip Detection Methods on Excitable Media

Authors: O.S. Chavan, T. Sunkel, and K.S. Wolf

This project has been conducted in the scope of the seminar "Nonlinear Dynamics, Time Series Analysis, and Machine Learning" from Feb 27 to Mar 10, 2023, offered by U Göttingen and MPI for Dynamics and Self-Organization, supervised by P. Bittihn and U. Parlitz (lecturers) and M. Aron (tutor for this project).
