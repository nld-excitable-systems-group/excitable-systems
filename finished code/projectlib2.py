import numpy as np
from scipy.ndimage import convolve
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as co
from scipy.signal import hilbert


import matplotlib.animation as animation
from IPython.display import HTML
from numba import jit
import timeit


def laplace(N,L):
    h = L/N
    L = 1/(6 * h*h) * np.array([
        [1, 4, 1],
        [4,-20, 4],
        [1, 4, 1]
    ])
    return L


def solve_system(func_du, func_dv, u_start, v_start, t_end, dt=1e-2, output_steps=50):
    '''
    solves differntial functions du/dt := func_du and dv/dt := func_dv
    for start conditions u_start and v_start (NxN Matrixe of the domain)
    t_end: endpoint of solution
    dt: time steps
    output_steps: how many equally distributed steps should be outputted
    '''
    U = []           # output
    V = []   
    
    u = u_start      # N*N Matrix with startvalues for excitation
    v = v_start
    
    t = 0
    t_output = np.linspace(0, t_end, output_steps)
    for t_next in t_output:
        while t < t_next:
            u = u + dt * func_du(u, v)
            v = v + dt * func_dv(u, v)
            t += dt
        U.append(u)
        V.append(v)
    return U, V



